local json = require("json")

-- Register all Toolbar actions and intialize all UI stuff
function initUi()
  app.registerUi({ ["menu"] = "Definition", ["callback"] = "getDefinition", ["accelerator"] = "<Alt>F1" });
end

function getSdcvCommand(word)
  -- TODO escape word to avoid code execution?
  -- TODO don't hardcode path
  return "/home/aaron/tools/sdcv/build/sdcv --utf8-input --utf8-output --json-output --non-interactive --data-dir ~/.config/xournalpp/plugins/definition/dictionaries/ -- \""
      .. word .. "\""
end

-- TODO make this get more than just first definition
function getFirstDefinition(jsonString)
  return json.decode(jsonString)[0]["definition"]
end

function getDefinitionText(selection)
  local output = io.popen(getSdcvCommand(selection))
  local first_definition = getFirstDefinition(output)
  return first_definition
end

-- Callback if the menu item is executed
function getDefinition()
  local selection = app.getSelectedTextFromPdf()
  print(selection) -- Why is this nil? Not getting the right data from C++
  local definition = getDefinitionText(selection)
  app.msgbox(definition)
end
